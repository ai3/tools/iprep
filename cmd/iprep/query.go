package main

import (
	"context"
	"flag"
	"fmt"
	"log"

	ipclient "git.autistici.org/ai3/tools/iprep/client"
	"github.com/google/subcommands"
	"google.golang.org/grpc"
)

type queryCommand struct {
	clientBase
}

func (c *queryCommand) Name() string     { return "query" }
func (c *queryCommand) Synopsis() string { return "query score for an IP" }
func (c *queryCommand) Usage() string {
	return `query [<flags>] <IP>:

        Query the score for the specified IP.

`
}

func (c *queryCommand) SetFlags(f *flag.FlagSet) {
	c.clientBase.SetFlags(f)
}

func (c *queryCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 1 {
		log.Printf("error: wrong number of arguments")
		return subcommands.ExitUsageError
	}
	if c.serverAddr == "" {
		log.Printf("error: must specify --server")
		return subcommands.ExitUsageError
	}

	if err := c.run(ctx, flag.Arg(0)); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	return subcommands.ExitSuccess
}

func (c *queryCommand) run(ctx context.Context, ip string) error {
	opts, err := c.clientBase.ClientOpts()
	if err != nil {
		return err
	}
	conn, err := grpc.Dial(c.serverAddr, opts...)
	if err != nil {
		return err
	}
	defer conn.Close()

	client := ipclient.New(conn)

	resp, err := client.GetScore(ctx, ip)
	if err != nil {
		return err
	}

	fmt.Printf("%g\n", resp.Score)

	return nil
}

func init() {
	subcommands.Register(&queryCommand{}, "")
}
