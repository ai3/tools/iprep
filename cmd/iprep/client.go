package main

import (
	"flag"
	"fmt"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

type clientBase struct {
	serverAddr string
	sslCA      string
}

func (c *clientBase) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.serverAddr, "server", "", "`address` (host:port) of the iprep server")
	f.StringVar(&c.sslCA, "ssl-ca", "", "`path` to a SSL CA certificate, enables SSL")
}

func (c *clientBase) ClientOpts() ([]grpc.DialOption, error) {
	var opts []grpc.DialOption

	if c.sslCA != "" {
		servername, _, err := net.SplitHostPort(c.serverAddr)
		if err != nil {
			return nil, fmt.Errorf("could not parse server addr: %w", err)
		}
		creds, err := credentials.NewClientTLSFromFile(c.sslCA, servername)
		if err != nil {
			return nil, err
		}
		opts = append(opts, grpc.WithTransportCredentials(creds))
	} else {
		opts = append(opts, grpc.WithInsecure())
	}

	return opts, nil
}
