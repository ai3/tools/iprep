package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"github.com/coreos/go-systemd/v22/daemon"
	"github.com/google/subcommands"
	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"gopkg.in/yaml.v3"

	"git.autistici.org/ai3/tools/iprep/ext"
	ippb "git.autistici.org/ai3/tools/iprep/proto"
	"git.autistici.org/ai3/tools/iprep/server"
)

type serverCommand struct {
	rpcAddr        string
	httpAddr       string
	dbURI          string
	scriptPath     string
	externalSrcDir string
	tlsCert        string
	tlsKey         string
	maskIPv4Bits   int
	maskIPv6Bits   int
}

func (c *serverCommand) Name() string     { return "server" }
func (c *serverCommand) Synopsis() string { return "run the rpc server" }
func (c *serverCommand) Usage() string {
	return `server [<flags>]:

        Run the main database and analysis service, with a GRPC API.

`
}

func (c *serverCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.rpcAddr, "grpc-addr", ":7170", "`address` of GRPC listener")
	f.StringVar(&c.httpAddr, "http-addr", ":7180", "`address` of HTTP debug listener")
	f.StringVar(&c.dbURI, "db", "leveldb:///var/lib/iprep/data", "database `uri` (sqlite:// or leveldb://)")
	f.StringVar(&c.scriptPath, "scoring-script", "/etc/iprep/score.td", "`path` to a custom scoring script")
	f.StringVar(&c.externalSrcDir, "ext-sources-dir", "/etc/iprep/external", "`path` to a directory containing external source definitions")

	f.StringVar(&c.tlsCert, "tls-cert", "", "TLS certificate `path` (grpc only)")
	f.StringVar(&c.tlsKey, "tls-key", "", "TLS private key `path` (grpc only)")
	f.IntVar(&c.maskIPv4Bits, "mask-ipv4-bits", 32, "bits for masking IPv4 addrs")
	f.IntVar(&c.maskIPv6Bits, "mask-ipv6-bits", 64, "bits for masking IPv6 addrs")
}

func (c *serverCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}

	if err := c.run(ctx); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	return subcommands.ExitSuccess
}

// Definition of an ExternalSource, represented as YAML.
type externalSourceSpec struct {
	Name   string                 `yaml:"name"`
	Type   string                 `yaml:"type"`
	Params map[string]interface{} `yaml:"params"`
}

// Read all files in externalSrcDir and build a map of ExternalSources.
func (c *serverCommand) loadExternalSources() (map[string]ext.ExternalSource, error) {
	files, err := filepath.Glob(filepath.Join(c.externalSrcDir, "*.yml"))
	if err != nil {
		return nil, err
	}

	m := make(map[string]ext.ExternalSource)
	for _, f := range files {
		data, err := ioutil.ReadFile(f)
		if err != nil {
			return nil, err
		}
		var spec externalSourceSpec
		if err := yaml.Unmarshal(data, &spec); err != nil {
			return nil, err
		}
		if spec.Name == "" {
			return nil, fmt.Errorf("error in external source spec %s: missing name", f)
		}
		if _, ok := m[spec.Name]; ok {
			return nil, fmt.Errorf("duplicate external source '%s'", spec.Name)
		}
		src, err := ext.New(spec.Type, spec.Params)
		if err != nil {
			return nil, fmt.Errorf("error creating external source '%s': %v", spec.Name, err)
		}
		m[spec.Name] = src
	}

	return m, nil
}

func (c *serverCommand) run(ctx context.Context) error {
	srcs, err := c.loadExternalSources()
	if err != nil {
		return err
	}

	mask := ippb.NewIPMask(c.maskIPv4Bits, c.maskIPv6Bits)
	srv, err := server.New(c.dbURI, c.scriptPath, srcs, mask)
	if err != nil {
		return err
	}
	defer srv.Close()

	var cancel context.CancelFunc
	ctx, cancel = context.WithCancel(ctx)
	g, ctx := errgroup.WithContext(ctx)

	g.Go(func() error {
		http.Handle("/metrics", promhttp.Handler())
		server := &http.Server{
			Addr:         c.httpAddr,
			ReadTimeout:  10 * time.Second,
			WriteTimeout: 30 * time.Second,
		}

		log.Printf("starting HTTP server on %s", c.httpAddr)
		return runHTTPServerWithContext(ctx, server)
	})

	g.Go(func() error {
		opts := []grpc.ServerOption{
			grpc.UnaryInterceptor(
				grpc_prometheus.UnaryServerInterceptor,
			),
			grpc.StreamInterceptor(
				grpc_prometheus.StreamServerInterceptor,
			),
		}
		if c.tlsCert != "" && c.tlsKey != "" {
			creds, err := credentials.NewServerTLSFromFile(c.tlsCert, c.tlsKey)
			if err != nil {
				return err
			}
			opts = append(opts, grpc.Creds(creds))
		}
		server := grpc.NewServer(opts...)
		ippb.RegisterIpRepServer(server, srv)
		grpc_prometheus.Register(server)
		grpc.EnableTracing = true

		log.Printf("starting GRPC server on %s", c.rpcAddr)
		return runGRPCServerWithContext(ctx, server, c.rpcAddr)
	})

	// Reload the scoring script on SIGHUP.
	reloadSigCh := make(chan os.Signal, 1)
	go func() {
		for range reloadSigCh {
			log.Printf("received SIGHUP, reloading configuration")
			srv.Reload()
		}
	}()
	signal.Notify(reloadSigCh, syscall.SIGHUP)

	// Terminate the program on SIGINT/SIGTERM.
	stopSigCh := make(chan os.Signal, 1)
	go func() {
		<-stopSigCh
		log.Printf("received termination signal, exiting")
		cancel()
	}()
	signal.Notify(stopSigCh, syscall.SIGINT, syscall.SIGTERM)

	// At this point we got nothing else to do but wait for the
	// serving goroutines to return. Notify systemd that we're
	// ready.
	daemon.SdNotify(false, daemon.SdNotifyReady) // nolint

	return g.Wait()
}

func runGRPCServerWithContext(ctx context.Context, server *grpc.Server, addr string) error {
	l, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}
	defer l.Close()

	go func() {
		<-ctx.Done()
		server.GracefulStop()
	}()

	return server.Serve(l)
}

func runHTTPServerWithContext(ctx context.Context, server *http.Server) error {
	go func() {
		<-ctx.Done()
		sctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		if err := server.Shutdown(sctx); err != nil {
			server.Close() // nolint: errcheck
		}
	}()

	err := server.ListenAndServe()
	if err == http.ErrServerClosed {
		return nil
	}
	return err
}

func init() {
	subcommands.Register(&serverCommand{}, "")
}
