package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strings"
	"time"

	ippb "git.autistici.org/ai3/tools/iprep/proto"
	"git.autistici.org/ai3/tools/iprep/submission"
	"github.com/google/subcommands"
	"google.golang.org/grpc"
)

type tailCommand struct {
	clientBase

	testOnly    bool
	patternFile string
	maxDelay    time.Duration
	maxStored   int
}

type pattern struct {
	rx        *regexp.Regexp
	eventType string
}

var patternRx = regexp.MustCompile(`^\s*/(.*)/\s+(\w+)\s*$`)

func parsePatterns(r io.Reader, filename string) ([]pattern, error) {
	var out []pattern
	scanner := bufio.NewScanner(r)
	var lineno int
	for scanner.Scan() {
		lineno++
		line := scanner.Text()
		if n := strings.Index(line, "#"); n >= 0 {
			line = line[:n]
		}
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}

		m := patternRx.FindStringSubmatch(line)
		if len(m) < 1 {
			return nil, fmt.Errorf("syntax error, %s line %d: no pattern found", filename, lineno)
		}
		p := strings.Replace(string(m[1]), "\\/", "/", -1)
		rx, err := regexp.Compile(p)
		if err != nil {
			return nil, fmt.Errorf("syntax error, %s line %d: %v", filename, lineno, err)
		}
		out = append(out, pattern{
			rx:        rx,
			eventType: string(m[2]),
		})
	}
	return out, scanner.Err()
}

func (c *tailCommand) Name() string     { return "tail" }
func (c *tailCommand) Synopsis() string { return "generate events from logs" }
func (c *tailCommand) Usage() string {
	return `tail [<flags>]:

        Monitor stdin in realtime and generate events based on
        user-specified detection rules (regexp-based). Meant to
        process third-party logs.

        The patterns file should contain one rule per line, consisting
        of a regexp bounded by / characters (Perl- style), followed by
        whitespace, followed by the event type that should be set on
        the generated event. Regular expressions must contain one
        capture group for the IP address.

        As an example, the following rule:

            /auth failure from ip ([.0-9]+)/ auth_failure

        will trigger an 'auth_failure' event with the IP extracted
        from the message.

`
}

func (c *tailCommand) SetFlags(f *flag.FlagSet) {
	c.clientBase.SetFlags(f)

	f.StringVar(&c.patternFile, "patterns", "", "`file` with patterns to load")
	f.BoolVar(&c.testOnly, "test", false, "only print matches, do not submit them")
	f.DurationVar(&c.maxDelay, "max-delay", 30*time.Second, "max queue delay")
	f.IntVar(&c.maxStored, "max-size", 1000, "max queue size")
}

func (c *tailCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}
	if c.serverAddr == "" {
		log.Printf("error: must specify --server")
		return subcommands.ExitUsageError
	}
	if c.patternFile == "" {
		log.Printf("error: must specify --pattern")
		return subcommands.ExitUsageError
	}

	if err := c.run(ctx); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	return subcommands.ExitSuccess
}

func (c *tailCommand) run(ctx context.Context) error {
	pf, err := os.Open(c.patternFile)
	if err != nil {
		return err
	}
	defer pf.Close()
	patterns, err := parsePatterns(pf, c.patternFile)
	if err != nil {
		return err
	}

	opts, err := c.clientBase.ClientOpts()
	if err != nil {
		return err
	}
	conn, err := grpc.Dial(c.serverAddr, opts...)
	if err != nil {
		return err
	}
	defer conn.Close()

	var sub submission.Submitter
	if !c.testOnly {
		sub = submission.New(conn, &submission.Options{
			MaxDelay:  c.maxDelay,
			MaxStored: c.maxStored,
		})
		defer sub.Close()
	}

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		b := scanner.Bytes()
		for _, p := range patterns {
			m := p.rx.FindSubmatch(b)
			if len(m) < 2 {
				continue
			}
			if c.testOnly {
				fmt.Printf("ip=%s type=%s\n", string(m[1]), p.eventType)
			} else {
				sub.AddEvent(&ippb.Event{
					Ip:    string(m[1]),
					Type:  p.eventType,
					Count: 1,
				})
			}
		}
	}

	return nil
}

func init() {
	subcommands.Register(&tailCommand{}, "")
}
