package main

import (
	"strings"
	"testing"
)

const testPatterns = `

### SSH authentication failures

# Silly brute-forcers that do not support our kex:
/^sshd\[\d+\]: Unable to negotiate with ([.0-9]+) port \d+: no matching host key type found./ ssh

### Email-related rules

# Postscreen failures - protocol errors are (in high volume) characteristic of spammers
/^postfix-in\/postscreen\[\d+\]: NOQUEUE: reject: RCPT from \[([.0-9]+)\]:\d+: 550 5.5.1 Protocol error;/ spammer

# Spammers trying to send email via disabled accounts
/^postfix-smtp-auth\/smtpd\[\d+\]: NOQUEUE: reject: RCPT from \[[^[]+(.[0-9]+)\]: 553 5.7.1 <[^>]+>: Sender address rejected: not owned by user/ spammer

# Spammers triggering SPF failures
/^policyd-spf\[\d+\]: 550 5.7.23 Message rejected due to: SPF fail - not authorized. Please see http:\/\/www.openspf.net\/Why?s=mfrom;id=[^;]*;ip=([.0-9]+);/ spammer

### Authentication

# General auth-server errors
/^auth-server\[\d+\]: auth: user=.* service=smtp status=error ip=([.0-9]+) error=/ auth

### Wordpress-specific rules

/^nginx\[\d+\]: .*nginx_access: \S+ \S+ ([.:0-9]+) .*"POST /wp-login\.php HTTP/ wordpress
/^nginx\[\d+\]: .*nginx_access: \S+ \S+ ([.:0-9]+) .*"POST /wp-comments-post\.php HTTP/ wordpress

`

func TestParsePatterns(t *testing.T) {
	patterns, err := parsePatterns(strings.NewReader(testPatterns), "test")
	if err != nil {
		t.Fatal(err)
	}
	if len(patterns) != 7 {
		t.Fatalf("bad result: %+v", patterns)
	}
}
