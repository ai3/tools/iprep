
CREATE TABLE events (
	ip TEXT,
	event_type TEXT,
	timestamp DATETIME,
        count INTEGER
);

CREATE INDEX idx_events_ip ON events (ip);
CREATE INDEX idx_events_type ON events (event_type);
-- CREATE INDEX idx_events_timestamp ON events (timestamp);
