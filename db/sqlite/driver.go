package sqlite

import (
	"database/sql"
	"strings"
	"time"

	ippb "git.autistici.org/ai3/tools/iprep/proto"
)

type DB struct {
	db   *sql.DB
	mask ippb.IPMask
}

func Open(path string, mask ippb.IPMask) (*DB, error) {
	// Auto-enable SQLite WAL.
	if !strings.Contains(path, "?") {
		path += "?_journal=WAL"
	}
	db, err := sqlOpen(path)
	if err != nil {
		return nil, err
	}

	return &DB{
		db:   db,
		mask: mask,
	}, nil
}

func (db *DB) Close() {
	db.db.Close()
}

func (db *DB) AddAggregate(aggr *ippb.Aggregate) error {
	tx, err := db.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare(
		"INSERT INTO events (ip, event_type, count, timestamp) VALUES (?, ?, ?, ?)")
	if err != nil {
		tx.Rollback() // nolint
		return err
	}
	defer stmt.Close()

	now := time.Now()

	for _, bt := range aggr.ByType {
		for _, item := range bt.ByIp {
			ip := db.mask.MaskString(item.Ip)
			if _, err := stmt.Exec(ip, bt.Type, item.Count, now); err != nil {
				tx.Rollback() // nolint
				return err
			}
		}
	}

	return tx.Commit()
}

func (db *DB) WipeOldData(age time.Duration) (int64, error) {
	res, err := db.db.Exec(
		"DELETE FROM events WHERE timestamp < ?",
		time.Now().Add(-age))
	if err != nil {
		return 0, err
	}
	n, _ := res.RowsAffected()
	return n, nil
}

func (db *DB) ScanIP(startTime time.Time, ip string) (map[string]int64, error) {
	tx, err := db.db.Begin()
	if err != nil {
		return nil, err
	}
	defer tx.Rollback() // nolint

	rows, err := tx.Query(
		`SELECT event_type, SUM(count) AS sum FROM events
                 WHERE ip = ? AND timestamp > ?
                 GROUP BY event_type`,
		db.mask.MaskString(ip), startTime)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	m := make(map[string]int64)
	for rows.Next() {
		var evType string
		var count int64
		if err := rows.Scan(&evType, &count); err != nil {
			return nil, err
		}
		m[evType] = count
	}

	return m, rows.Err()
}

func (db *DB) ScanType(startTime time.Time, t string) (map[string]int64, error) {
	return nil, nil
}

func (db *DB) ScanAll(startTime time.Time, f func(string, map[string]int64) error) error {
	tx, err := db.db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback() // nolint

	rows, err := tx.Query(
		`SELECT ip, event_type, SUM(count) AS sum FROM events
                 WHERE timestamp > ?
                 GROUP BY ip, event_type
                 ORDER BY ip ASC`,
		startTime)
	if err != nil {
		return err
	}
	defer rows.Close()

	// Iterate over the query results. Since all data for an IP is
	// consecutive, we acumulate it into a map and call 'f' once
	// we detect a change in the IP.
	var m map[string]int64
	var curIP string
	for rows.Next() {
		var ip, evType string
		var count int64
		if err := rows.Scan(&ip, &evType, &count); err != nil {
			return err
		}

		if ip != curIP {
			if curIP != "" {
				if err := f(curIP, m); err != nil {
					return err
				}
			}
			curIP = ip
			m = make(map[string]int64)
		}

		m[evType] = count
	}

	// Flush the last map, if any.
	if len(m) > 0 {
		if err := f(curIP, m); err != nil {
			return err
		}
	}

	return rows.Err()
}
