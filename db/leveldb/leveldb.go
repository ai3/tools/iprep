package leveldb

import (
	"bytes"
	"encoding/binary"
	"log"
	"net"
	"time"

	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/errors"
	"github.com/syndtr/goleveldb/leveldb/filter"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"github.com/syndtr/goleveldb/leveldb/util"

	ippb "git.autistici.org/ai3/tools/iprep/proto"
)

var (
	eventPrefix     = []byte("e/")
	ipIndexPrefix   = []byte("i/ip/")
	typeIndexPrefix = []byte("i/t/")
)

const deleteBatchSize = 10000

type DB struct {
	db   *leveldb.DB
	mask ippb.IPMask
}

func Open(path string, mask ippb.IPMask) (*DB, error) {
	opts := &opt.Options{
		BlockCacheCapacity: 64 * opt.MiB,
		WriteBuffer:        64 * opt.MiB,
		Filter:             filter.NewBloomFilter(30),
	}
	db, err := leveldb.OpenFile(path, opts)
	if errors.IsCorrupted(err) {
		log.Printf("database appears to be corrupted: %v, attempting recovery...", err)
		db, err = leveldb.RecoverFile(path, opts)
	}
	if err != nil {
		return nil, err
	}
	return &DB{
		db:   db,
		mask: mask,
	}, nil
}

func (db *DB) Close() {
	db.db.Close()
}

func (db *DB) AddAggregate(aggr *ippb.Aggregate) error {
	var ev ippb.Event

	wb := new(leveldb.Batch)
	for _, bt := range aggr.ByType {
		for _, item := range bt.ByIp {
			id := nextID()

			ev.Type = bt.Type
			ev.Ip = item.Ip
			ev.Count = item.Count
			enc := marshalEvent(&ev, db.mask)

			wb.Put(eventKeyFromID(id), enc.data())
			wb.Put(indexKey(id, ipIndexPrefix, enc.binaryIP()), enc.data())
			wb.Put(indexKey(id, typeIndexPrefix, enc.eventType()), enc.data())
		}
	}

	return db.db.Write(wb, nil)
}

func (db *DB) delete(endTime time.Time) (int64, error) {
	iter := db.db.NewIterator(
		prefixUntil(endTime, eventPrefix),
		&opt.ReadOptions{
			DontFillCache: true,
		})
	defer iter.Release()

	// We might have a lot of data to delete, and we would like to
	// do so without the risk of running out of memory due to a
	// huge Batch. So we just flush the Batch as we go once it's
	// reached a certain size limit.
	wb := new(leveldb.Batch)
	batchSize := 0
	var count int64

	for iter.Next() {
		e := encodedEvent(iter.Value())
		id := eventIDFromKey(iter.Key())
		wb.Delete(iter.Key())
		wb.Delete(indexKey(id, ipIndexPrefix, e.binaryIP()))
		wb.Delete(indexKey(id, typeIndexPrefix, e.eventType()))

		count++
		batchSize++
		if batchSize > deleteBatchSize {
			if err := db.db.Write(wb, nil); err != nil {
				return count, err
			}
			wb.Reset()
			batchSize = 0
		}
	}

	if batchSize == 0 {
		return count, nil
	}
	return count, db.db.Write(wb, nil)
}

func (db *DB) WipeOldData(age time.Duration) (int64, error) {
	endTime := time.Now().Add(-age)
	n, err := db.delete(endTime)
	if err != nil {
		return n, err
	}
	return n, db.db.CompactRange(util.Range{})
}

func (db *DB) ScanIP(startTime time.Time, ip string) (map[string]int64, error) {
	iter := db.db.NewIterator(
		prefixSince(startTime, ipIndexPrefix, ipToBytes(ip, db.mask)),
		nil)
	defer iter.Release()

	m := make(map[string]int64)
	for iter.Next() {
		e := encodedEvent(iter.Value())
		m[string(e.eventType())] += e.count()
	}
	return m, iter.Error()
}

func (db *DB) ScanType(startTime time.Time, t string) (map[string]int64, error) {
	iter := db.db.NewIterator(
		prefixSince(startTime, typeIndexPrefix, []byte(t)),
		&opt.ReadOptions{
			DontFillCache: true,
		})
	defer iter.Release()

	m := make(map[string]int64)
	for iter.Next() {
		e := encodedEvent(iter.Value())
		m[e.ip()] += e.count()
	}
	return m, iter.Error()
}

func (db *DB) ScanAll(startTime time.Time, f func(string, map[string]int64) error) error {
	iter := db.db.NewIterator(
		util.BytesPrefix(ipIndexPrefix),
		&opt.ReadOptions{
			DontFillCache: true,
		})

	// Iterate over the IP index, skipping over entries outside of
	// the desired time range. Since all data for an IP is
	// consecutive, we acumulate it into a map and call 'f' once
	// we detect a change in the IP.
	var m map[string]int64
	var curIP string
	for iter.Next() {
		key := iter.Key()
		ts := timestampFromIPIndexKey(key)
		if ts.Before(startTime) {
			continue
		}

		ip := ipFromIPIndexKey(key).String()
		if ip != curIP {
			if curIP != "" {
				if err := f(curIP, m); err != nil {
					return err
				}
			}
			curIP = ip
			m = make(map[string]int64)
		}

		e := encodedEvent(iter.Value())
		m[string(e.eventType())] += e.count()
	}

	// Flush the last map, if any.
	if len(m) > 0 {
		if err := f(curIP, m); err != nil {
			return err
		}
	}

	return iter.Error()
}

func (db *DB) Compact() error {
	return db.db.CompactRange(util.Range{})
}

func eventKeyFromID(id []byte) []byte {
	return append(eventPrefix, id...)
}

func eventIDFromKey(key []byte) []byte {
	return key[len(eventPrefix):]
}

func indexKey(id, indexPrefix, indexKey []byte) []byte {
	return bytes.Join([][]byte{indexPrefix, indexKey, id}, nil)
}

func timestampFromIPIndexKey(key []byte) time.Time {
	nano := binary.BigEndian.Uint64(key[len(ipIndexPrefix)+16 : len(ipIndexPrefix)+24])
	return time.Unix(0, int64(nano))
}

func ipFromIPIndexKey(key []byte) net.IP {
	return net.IP(key[len(ipIndexPrefix) : len(ipIndexPrefix)+16])
}

func ipToBytes(s string, mask ippb.IPMask) []byte {
	return []byte(mask.MaskIP(net.ParseIP(s)).To16())
}

func prefixSince(start time.Time, indexPrefix, indexKey []byte) *util.Range {
	pfx := append(indexPrefix, indexKey...)
	b := make([]byte, len(pfx)+8)
	copy(b[:len(pfx)], pfx)
	binary.BigEndian.PutUint64(b[len(pfx):len(pfx)+8], uint64(start.UnixNano()))
	pfx[len(pfx)-1]++
	return &util.Range{
		Start: b,
		Limit: pfx,
	}
}

func prefixUntil(end time.Time, pfx []byte) *util.Range {
	b := make([]byte, len(pfx)+8)
	copy(b[:len(pfx)], pfx)
	binary.BigEndian.PutUint64(b[len(pfx):len(pfx)+8], uint64(end.UnixNano()))
	return &util.Range{
		Limit: b,
	}
}

// A fast binary encoding for an Event, to avoid calling proto.Unmarshal.
// The first 16 bytes are the IP, followed by 8 bytes of counter, followed
// by the (free form) type field.
type encodedEvent []byte

func marshalEvent(e *ippb.Event, mask ippb.IPMask) encodedEvent {
	b := make([]byte, 16+8)
	copy(b[:16], ipToBytes(e.Ip, mask))
	binary.BigEndian.PutUint64(b[16:24], uint64(e.Count))
	btype := []byte(e.Type)
	return encodedEvent(append(b, btype...))
}

func (e encodedEvent) binaryIP() []byte {
	return e[:16]
}

func (e encodedEvent) ip() string {
	return net.IP(e[:16]).String()
}

func (e encodedEvent) count() int64 {
	return int64(binary.BigEndian.Uint64(e[16:24]))
}

func (e encodedEvent) eventType() []byte {
	return e[24:]
}

func (e encodedEvent) data() []byte {
	return []byte(e)
}
