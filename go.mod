module git.autistici.org/ai3/tools/iprep

go 1.14

require (
	github.com/coreos/go-systemd/v22 v22.3.2
	github.com/d5/tengo/v2 v2.7.0
	github.com/go-kit/kit v0.10.0 // indirect
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/golang/protobuf v1.5.2
	github.com/google/go-cmp v0.5.5
	github.com/google/subcommands v1.2.0
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/mattn/go-sqlite3 v1.14.7
	github.com/oschwald/maxminddb-golang v1.8.0
	github.com/prometheus/client_golang v1.11.0
	github.com/syndtr/goleveldb v1.0.0
	golang.org/x/net v0.0.0-20210420072503-d25e30425868 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	google.golang.org/genproto v0.0.0-20210416161957-9910b6c460de // indirect
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.26.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
