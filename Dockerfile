FROM golang:1.16 AS build

ADD . /src
WORKDIR /src
RUN apt-get -q update && apt-get install -y libsqlite3-dev
RUN env CGO_LDFLAGS=-lm go build -ldflags "-linkmode external -extldflags -static" -tags "netgo libsqlite3" -o /iprep ./cmd/iprep

FROM scratch
COPY --from=build /iprep /iprep

ENTRYPOINT ["/iprep", "server"]
