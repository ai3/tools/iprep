package geoip

import (
	"net"

	tengo "github.com/d5/tengo/v2"
	"github.com/oschwald/maxminddb-golang"
)

var defaultGeoIPPaths = []string{
	"/var/lib/GeoIP/GeoLite2-Country.mmdb",
}

type GeoIP struct {
	readers []*maxminddb.Reader
}

func New(paths []string) (*GeoIP, error) {
	if len(paths) == 0 {
		paths = defaultGeoIPPaths
	}

	g := new(GeoIP)
	for _, path := range paths {
		r, err := maxminddb.Open(path)
		if err != nil {
			return nil, err
		}
		g.readers = append(g.readers, r)
	}
	return g, nil
}

func (g *GeoIP) LookupIP(ipStr string) (tengo.Object, error) {
	ip := net.ParseIP(ipStr)
	var record struct {
		Country struct {
			ISOCode string `maxminddb:"iso_code"`
		} `maxminddb:"country"`
	}
	for _, r := range g.readers {
		if err := r.Lookup(ip, &record); err == nil {
			return &tengo.String{Value: record.Country.ISOCode}, nil
		}
	}
	return tengo.UndefinedValue, nil
}
